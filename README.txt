#
# wget -qO- "https://gitlab.com/ezze-public/crypto-market-history/-/raw/main/README.txt?ref_type=heads" | bash

docker system prune -a -f

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker-installer/docker-installer.sh | bash
git clone https://gitlab.com/ezze-public/crypto-market-history.git /docker/cmh
sed -i 's/https:\/\/gitlab.com\//git@gitlab.com:/g' /docker/cmh/.git/config

wget -O dfphp "https://gitlab.com/ezze-public/conf/-/raw/main/php8-on-docker/conf/dockerfile?ref_type=heads"
cat dfphp | grep -v '0.0.0.0:9000' | grep -v EXPOSE > dockerfile
sed -i "s,#INJECT,RUN wget -qO- https://gitlab.com/ezze-public/crypto-market-history/-/raw/main/conf/patch | bash,g" dockerfile

docker build -t cmh-image -f dockerfile .
docker run -t -d --restart unless-stopped --name cmh --hostname cmh -p 8022:22 -p 8023:8023 -v /docker/cmh:/var/www cmh-image

rm -rf dockerfile dfphp /var/www/README.md /var/www/conf

# finally
# https://gitlab.com/ezze-private/rsa-key-access/-/blob/main/git-rsa/README.txt?ref_type=heads


