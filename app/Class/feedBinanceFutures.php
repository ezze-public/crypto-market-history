<?php

namespace App\Class;

use Illuminate\Support\Facades\Storage;
use ZipArchive;
use Illuminate\Http\File;


class feedBinanceFutures
{
    

    private static $timeframe_s = [ '1m', '3m', '5m', '15m', '30m', '1h', '2h', '4h', '6h', '8h', '12h', '1d' ]; // , '3d', '1w', '1M'

        
    public static function add( $pair, $tf ){
        
        $date = gmdate('Y-m-d', gmdate('U')-3600*24);
        
        # check if such pair exists on binance
        if(! $header_s = get_headers("https://data.binance.vision/data/futures/um/daily/klines/{$pair}/{$tf}/${pair}-{$tf}-{$date}.zip") )
            return false;

        if(! strstr($header_s[0] , '200') )
            return false;

        # if already added ignore it
        if( file_exists( storage_path("app/binance/futures/{$pair}/{$tf}") ) )
            return true;
        
        # get all tf in background
        echo shell_exec(" cd .. && php artisan feed:first --exchange binance --market futures --pair {$pair} --tf {$tf} >/dev/null 2>&1 & ");
        return true;

    }


    public static function first( $pair, $tf ){

        $pair = etc::pair_normalize($pair);

        if(! in_array($tf, self::$timeframe_s) ){
            echo "wrong pair: {$pair} or tf: {$tf}\n";
            die;
        }

        $base = storage_path("app/binance/futures");
        $date = gmdate('Y-m-d', gmdate('U') - 24*3600 );
        
        # check if already done
        if( file_exists("{$base}/{$pair}/{$tf}/{$pair}-{$tf}-{$date}.csv") ){
            echo "already got the pair {$pair} at tf {$tf}\n";


        # check if such pair exists on binance
        } else if(! $header_s = get_headers("https://data.binance.vision/data/futures/um/daily/klines/{$pair}/{$tf}/${pair}-{$tf}-{$date}.zip") ){
            return false;

        } else if(! strstr($header_s[0] , '200') ){
            echo "Wrong pair: {$pair} or tf: {$tf}\n";
            return false;

        } else {
            etc::take_care_of_path( storage_path("app/binance/futures/{$pair}/{$tf}") );
            self::add_from_ancient($pair, $tf);
        }

    }


    public static function sync(){

        $d_base = storage_path('app/binance/futures/');
        $date = gmdate('Y-m-d');

        foreach( glob($d_base."*") as $pair ){

            $pair = basename($pair);
            // BTCUSDT, ...
            
            if( substr($pair, 0, 1) == '.' )
                continue;

            $d_pair = $d_base.$pair."/";
            
            foreach( glob($d_pair."*") as $tf ){

                $tf = basename($tf);
                // 5m, 4h, ...
                
                if(! in_array($tf, self::$timeframe_s) )
                    continue;

                $d_tf = $d_pair.$tf."/";

                $curr = shell_exec(" ls {$d_tf} 2>/dev/null | tail -1 "); // BTCUSDT-5m-2023-02-15.csv
                $curr = trim($curr, "\r\n\t ");
                
                if(! $curr ){
                    // echo "tf is empty\n";
                    self::add_from_ancient($pair, $tf);

                } else {

                    $curr = explode("{$pair}-{$tf}-", $curr)[1];
                    $curr = substr($curr, 0, -4); // 2023-02-15

                    while( true ){
                        
                        $curr = gmdate('Y-m-d', strtotime($curr) + 24*3600); // next day
                        
                        if( $curr >= $date )
                            break;
                        
                        $res = self::csv_get($pair, $tf, $curr);

                        if( $res === true ){
                            echo "{$pair}:{$tf} $curr +\n";
                        
                        } else if( $res === 404 ){
                            break;
                        
                        } else if( $res === 503 ){
                            echo "wrong compressed file content\n";
                            die;
                        }

                    }
                
                }


            }
            
        }

    }


    public static function add_from_ancient( $pair, $tf ){
        
        etc::ps_semaphore(['php', 'artisan', 'feed:first'], 6 );

        $url = "https://s3-ap-northeast-1.amazonaws.com/data.binance.vision?delimiter=/&prefix=data/futures/um/daily/klines/{$pair}/{$tf}/";
        $tf_done = false;

        while( true ){
            
            // as the output of amazon, is pagnated and its getting the page number form the "marker" parameter, so we use the obj->NextMarket 
            // to define the number of the page we are asking data for.
            $xml = file_get_contents( $url . ( isset($obj) && $obj->NextMarker ? '&marker='.urlencode($obj->NextMarker) : '' ) );
            $obj = simplexml_load_string($xml);

            foreach( $obj->Contents as $item ){

                $key = $item->Key;
                // echo $key; die;
                
                // it returns two type of file address {zip and checksum of zip}, so we ignore the checksum addresses.
                if( strrchr($key, '.') != '.zip' )
                    continue;

                $date = strrchr($key, '/');
                $date = explode('.', $date)[0];
                $date = substr($date, -10);

                if( self::csv_have($pair, $tf, $date) ){
                    echo "{$pair}:{$tf} $date .\n";

                } else {
                    
                    $res = self::csv_get($pair, $tf, $date);

                    if( $res === true ){
                        echo "{$pair}:{$tf} $date +\n";
                    
                    } else if( $res === 404 ){
                        $tf_done = true;
                    
                    } else if( $res === 503 ){
                        echo "{$pair}:{$tf} $date wrong compressed file content\n";
                        die;
                    }

                }

                if( $tf_done )
                    break;
                
            }

            // no more item in this tf
            if( $tf_done ){
                echo "no more item in {$pair}:{$tf}\n";
                break;
            }

            // if there is no "next page" close the while, and go to next step
            if(! $obj->NextMarker )
                break;
            
        }

    }


    private static function csv_have( $pair, $tf, $date ){

        $base = storage_path('app/binance/futures/');
        $the_csv = "{$pair}-{$tf}-{$date}.csv";
        
        return file_exists($base.$pair.'/'.$tf.'/'.$the_csv);

    }


    public static function csv_get( $pair, $tf, $date ){
        
        $temp = "/tmp/";
        $the_zip = "{$pair}-{$tf}-{$date}.zip";
        $the_csv = "{$pair}-{$tf}-{$date}.csv";

        $path = 'binance/futures/'.$pair.'/'.$tf.'/';
        $the_url = "https://data.binance.vision/data/futures/um/daily/klines/{$pair}/{$tf}/{$the_zip}";

        if(! @copy($the_url, $temp.$the_zip) ){
            echo "404 on {$the_url}\n";
            return 404;
        
        } else {
            
            $zip = new ZipArchive;
            $res = $zip->open($temp.$the_zip);

            if( $res === true ){
                
                $zip->extractTo($temp);
                $zip->close();
                unlink($temp.$the_zip);
                Storage::disk('local')->put($path.$the_csv, file_get_contents($temp.$the_csv), 'public');
                unlink($temp.$the_csv);

                echo $the_csv."\n";

                return true;

            } else {
                echo "res fault\n";
                return 502;
            }
        
        }

    }
    

}
