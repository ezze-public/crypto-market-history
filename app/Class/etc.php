<?php

namespace App\Class;

class etc
{


    public static function take_care_of_path( $path ){
        
        if(! file_exists($path) )
            shell_exec(" mkdir -p {$path} ");

        return $path;

    }
    

    public static function ps_semaphore( $proc_pattern, $die_or_wait=true ){
        
        $grep_str = '';

        if( is_array($proc_pattern) ){
            foreach( $proc_pattern as $pp )
                $grep_str.= "| grep '{$pp}' ";

        } else {
            $grep_str = "| grep '{$proc_pattern }' ";
        }
        
        $command = " sudo ps aux {$grep_str}| grep -v 'ps aux' | grep -v grep | grep -v 'sh -c' | awk {'print $2'} | grep -v ".getmypid()." | wc -l ";

        while (true) {

            $curr_count = trim( shell_exec($command), "\r\n\t ");

            if( $curr_count == 0 )
                return true;

            if( $die_or_wait === true ){
                echo "already running ..\n";
                die;
            
            } else {
                $time = is_numeric($die_or_wait) ? $die_or_wait : 1;
                echo __FUNCTION__.": sleeping for $time seconds ..\n";
                sleep($time);
            }
        
        }

    }


    public static function pair_normalize( $pair ){
        
        if(! is_null($pair) ){
            $pair = strtoupper(preg_replace("/[^a-zA-Z0-9]/", "", $pair));
            $pair = strtoupper($pair);
        }

        return $pair;

    }


}
