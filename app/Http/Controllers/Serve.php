<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Class\etc;

class Serve extends Controller
{
    
    
    public function memo(){
        
        return '<pre>'.
            '/serve/{exchange}/{market}/{pair}/{tf}/{date_from}/{date_to}'.
            '</pre>';

    }


    public function serve_data( $exchange, $market, $pair, $tf, $date_from, $date_to ){
        
        // exchange: binance
        // market: futures, spot
        // pair: btcusdt, ..
        // tf: 1m, 5m, 4h, ..
        // date_from: 2020-11-21_20:11:00
        // date_to: 2021-05-10_10:13:11

        // validate
        // ** this section should be done by later

        $pair = etc::pair_normalize($pair);

        $dates = self::period_date($date_from, $date_to);
        $base = storage_path("app/{$exchange}/{$market}/");
        $csv = '';
        $i = 0;
        $end = count($dates) - 1;

        if (count($dates) == 0)
            return response()->json(['status'=> 'ER', 'msg'=> 'wrong date range']);
        
        $res[] = [ 'date_start', 'open', 'low', 'high', 'close', 'end_date' ];

        foreach( $dates as $date ){
            
            $skip_time = [];
            
            if ($end == 0) {
                $skip_time = ['skip'=> 'before/after', 'time'=> [strtotime($date_from) * 1000, strtotime($date_to) * 1000]];
            
            } else {
                
                if( $i == 0 ){
                    $skip_time = ['skip'=> 'before', 'time'=> strtotime($date_from) * 1000];
                
                } else if( $i == $end ){
                    $skip_time = ['skip'=> 'after', 'time'=> strtotime($date_to) * 1000];
                
                } else {
                    $skip_time = ['skip'=> ''];
                }

            }

            $path = "{$base}{$pair}/{$tf}/{$pair}-{$tf}-{$date}.csv";
            
            if(! file_exists( $path ) ){
                $class = "App\\Class\\feed".ucfirst($exchange).ucfirst($market);
                $class::csv_get($pair, $tf, $date);
                if(! file_exists( $path ) )
                    die("Can't get the csv: {$path}\n");
            }

            $csv = self::csv_read($path, $skip_time);  
            
            unset($skip_time);
            $res = array_merge($res, $csv);

            $i++;

        }
        
        return response()->json(['status'=>'OK', 'res'=>$res]);

    }


    private function open_and_merge_the_csv_files( $list, $date_from, $date_to ){
        return $list;
    }


    private function period_date( $date_from, $date_to ){
        
        $rang_array = [];
        $date_from = strtotime($date_from);
        $date_to = strtotime($date_to);
        
        for ($current_date = $date_from; $current_date <= $date_to; 
                $current_date += (86400)) {
                $date = date('Y-m-d', $current_date);
                $rang_array[] = $date;
        }
        
        return $rang_array;

    }


    private function csv_read( $path, $skip_time ){

        if ( ( $open = fopen( $path, "r" ) ) !== false) {
            
            $i = 0;
            
            while ( ( $data = fgetcsv($open, filesize($path), "," ) ) !== false ) {
                if ($i == 0 && $data[0] == 'open_time') continue;
                $data = array_slice($data, 0, 7, true);
                unset($data[5]);
                if ($skip_time['skip'] == 'before/after' && ($data[0] < $skip_time['time'][0] || $data[0] > $skip_time['time'][1])) continue;
                if ($skip_time != [] && $skip_time['skip'] == 'before' && $data[0] < $skip_time['time']) continue;
                if ($skip_time != [] && $skip_time['skip'] == 'after' && $data[0] > $skip_time['time']) continue;
                $res[] = [$data[0], $data[1], $data[2], $data[3], $data[4], $data[6]];
                $i++;
            }

            fclose( $open );
            return $res;

        }

        return false;

    }

    
}
