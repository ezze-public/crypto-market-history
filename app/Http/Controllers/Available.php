<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Class\etc;

class Available extends Controller
{
    

    public function memo(){
        return "<pre>".
            // "/available/{exchange}/{market}\n".
            // "/available/{exchange}/{market}/{pair}\n".
            "/available/{exchange}/{market}/{pair}/{tf}".
            "</pre>";
    }
    

    public function available( $exchange, $market, $pair=false ){

        $pair = etc::pair_normalize($pair);

        $base = storage_path("app/{$exchange}/{$market}");        
        $date = gmdate('Y-m-d', gmdate('U') - 24*3600 );

        $dirs = $res = [];
        
        if ($pair == false) {
            $main_dir = "{$exchange}/{$market}/";
            $dirs = Storage::disk('local')->directories($main_dir);
            foreach ($dirs as $dir) {
                $res[] = str_replace($main_dir, '', $dir);
            }
            
        } else {
            $main_dir = "{$exchange}/{$market}/". strtoupper($pair) ."";
            $tf_s = Storage::disk('local')->directories($main_dir);
            foreach ($tf_s as $tf) {
                $tf = str_replace($main_dir.'/', '', $tf);
                if( file_exists("{$base}/{$pair}/{$tf}/{$pair}-{$tf}-{$date}.csv") ){
                    $res[] = $tf;
                }
            }
        }

        return response()->json(['status'=>'OK', 'res'=>$res]);

    }


    public function available_tf($exchange, $market, $pair, $tf){

        $pair = etc::pair_normalize($pair);

        $base = storage_path("app/{$exchange}/{$market}");
        $date = gmdate('Y-m-d', gmdate('U') - 24*3600 );
        
        # check if its up-to-yesterday
        if(! file_exists("{$base}/{$pair}/{$tf}/{$pair}-{$tf}-{$date}.csv") )
            return response()->json(['status'=>'ER', 'msg'=>"Can't find the related csv file"]);

        $main_dir = "{$exchange}/{$market}/".strtoupper($pair)."/{$tf}";
        
        $files = Storage::disk('local')->files($main_dir);
        sort($files);

        $remove_str = [$main_dir.'/', '.csv', strtoupper($pair).'-', $tf.'-'];
        
        $res = [
            'date_from'=> str_replace($remove_str, '', $files[0]).'T00:00',
            'date_to'=> str_replace($remove_str, '', end($files)).'T23:59',
        ];
        
        return response()->json(['status'=>'OK', 'res'=>$res]);

    }
    

}
