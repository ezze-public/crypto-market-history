<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Main extends Controller
{
    
    public function memo(){
        
        $res = '<pre>';
        
        foreach( ['serve', 'available', 'need'] as $c )
            $res.= "/$c\n";
        
        $res.= '</pre>';

        return $res;

    }

}
