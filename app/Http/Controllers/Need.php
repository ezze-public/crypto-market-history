<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Class\feedBinanceFutures;
use App\Class\etc;

class Need extends Controller
{
    

    public function memo(){
        return '<pre>'.
            '/need/{exchange}/{market}/{pair}/{tf}'.
            '</pre>';
    }
    
    
    public function add( $exchange, $market, $pair, $tf ){
        
        $pair = etc::pair_normalize($pair);
        $class = "App\\Class\\feed".ucfirst($exchange).ucfirst($market);

        if(! class_exists($class) ){
            $data = [ 'status' => 'ER', 'msg' => "Wrong exchange: {$exchange} or market: {$market}" ];

        } else if(! $class::add($pair, $tf) ){
            $data = [ 'status' => 'ER', 'msg' => "Wrong pair: {$pair} or tf: {$tf}" ];

        } else {
            $data = [
                'status' => 'OK', 
                'res' => [
                    'exchange' => $exchange,
                    'market' => $market,
                    'pair' => $pair,
                    'tf' => $tf
                ]
            ];
        }

        return response()->json($data);
        
    }


}
