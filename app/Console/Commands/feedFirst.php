<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Class\feedBinanceFutures;
use App\Class\etc;

# php artisan feed:first --exchange binance --market futures --pair BTCUSDT --tf 5m

class feedFirst extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:first {--exchange=} {--market=} {--pair=} {--tf=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'I\'ll tell you later.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $exchange = $this->option('exchange');
        $market = $this->option('market');
        $pair = strtoupper($this->option('pair'));
        $tf = $this->option('tf');

        $class = "App\\Class\\feed".ucfirst($exchange).ucfirst($market);
        $class::first($pair, $tf);

        return Command::SUCCESS;

    }
    
}
