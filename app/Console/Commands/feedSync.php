<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Class\feedBinanceFutures;
use App\Class\etc;

# php artisan feed:sync --exchange binance --market futures

class feedSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:sync {--exchange=} {--market=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'I\'ll tell you later.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        etc::ps_semaphore(['php', 'artisan', 'feed:first']);
        etc::ps_semaphore(['php', 'artisan', 'feed:sync']);

        $exchange = $this->option('exchange');
        $market = $this->option('market');

        $class = "App\\Class\\feed".ucfirst($exchange).ucfirst($market);
        $class::sync();

        return Command::SUCCESS;

    }
    
}
