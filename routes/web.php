<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Available;
use App\Http\Controllers\Serve;
use App\Http\Controllers\Need;
use App\Http\Controllers\Main;
use App\Class\feedBinanceFutures;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [Main::class, 'memo']);

Route::get('/serve', [Serve::class, 'memo']);
Route::get('/serve/{exchange}/{market}/{pair}/{tf}/{date_from}/{date_to}', [Serve::class, 'serve_data']);

Route::get('/available', [Available::class, 'memo']);
// Route::get('/available/{exchange}/{market}', [Available::class, 'available']);
// Route::get('/available/{exchange}/{market}/{pair}', [Available::class, 'available']);
Route::get('/available/{exchange}/{market}/{pair}/{tf}', [Available::class, 'available_tf']);

Route::get('/need', [Need::class, 'memo']);
Route::get('/need/{exchange}/{market}/{pair}/{tf}', [Need::class, 'add']);

